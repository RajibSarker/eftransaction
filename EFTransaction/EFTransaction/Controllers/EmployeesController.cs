﻿using EFTransaction.Database;
using EFTransaction.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EFTransaction.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly ApplicationDbContext db;

        public EmployeesController(ApplicationDbContext db)
        {
            this.db = db;
        }




        // GET: api/<EmployeesController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<EmployeesController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<EmployeesController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] EmployeeSalary employeeSalary)
        {
            #region not using Transaction scope
            //await db.Employees.AddAsync(employeeSalary.Employee);
            //await db.SaveChangesAsync();

            //throw new Exception("Exception after adding employee info");

            //employeeSalary.Salary.EmployeeId = employeeSalary.Employee.Id;
            //await db.Salaries.AddAsync(employeeSalary.Salary);
            //await db.SaveChangesAsync();
            #endregion

            #region using TS
            using (IDbContextTransaction ts = db.Database.BeginTransaction())
            {
                try
                {
                    await db.Employees.AddAsync(employeeSalary.Employee);
                    await db.SaveChangesAsync();

                    //throw new Exception("Exception after adding employee info");

                    employeeSalary.Salary.EmployeeId = employeeSalary.Employee.Id;
                    await db.Salaries.AddAsync(employeeSalary.Salary);
                    await db.SaveChangesAsync();

                    ts.Commit();
                }
                catch (Exception ex)
                {
                    ts.Rollback();
                    throw new Exception(ex.Message);
                }
            }
            #endregion



            return Ok(employeeSalary.Employee);

        }

        // PUT api/<EmployeesController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EmployeesController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
