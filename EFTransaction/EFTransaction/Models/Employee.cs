﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFTransaction.Models
{
    [Table("Employees")]
    public class Employee
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public DateTime JoiningDate { get; set; }
    }
}
