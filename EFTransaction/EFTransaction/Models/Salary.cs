﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EFTransaction.Models
{
    [Table("Salaries")]
    public class Salary
    {
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public decimal GrossSalary { get; set; }
    }
}
