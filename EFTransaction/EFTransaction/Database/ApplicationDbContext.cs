﻿using EFTransaction.Models;
using Microsoft.EntityFrameworkCore;

namespace EFTransaction.Database
{
    public class ApplicationDbContext:DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options):base(options)
        {

        }

        public DbSet<Employee> Employees{ get; set; }
        public DbSet<Salary> Salaries{ get; set; }

    }
}
